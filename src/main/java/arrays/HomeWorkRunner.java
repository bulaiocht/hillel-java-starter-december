package arrays;

import java.util.Arrays;
import java.util.Random;

public class HomeWorkRunner {

    public static void main(String[] args) {

        int [] randomArray = new int [10];

        randomizeArray(randomArray, -99, 99);

        System.out.println(Arrays.toString(randomArray));

        int min = randomArray[0];
        int minIndex = 0;

        int max = randomArray [0];
        int maxIndex = 0;

        for (int i = 0; i < randomArray.length; i++) {

            if (randomArray[i] <= min) {
                min = randomArray[i];
                minIndex = i;
                continue;
            }

            if (randomArray[i] >= max) {
                max = randomArray[i];
                maxIndex = i;
            }

        }

        System.out.printf("Min number: %s \n Max number: %s \n", min, max);
        System.out.printf("Min index: %s \n Max index: %s \n", minIndex, maxIndex);

        swap(randomArray, minIndex, maxIndex);

        System.out.println(Arrays.toString(randomArray));

        inverse(randomArray);

        System.out.println(Arrays.toString(randomArray));

        Arrays.sort(randomArray);

        System.out.println(Arrays.toString(randomArray));

        int last = randomArray.length - 1;

        int numberOfMaxElements = 3;

        for (int i = last; i > last - numberOfMaxElements; i--) {
            System.out.println(randomArray[i]);
        }

        int[] maxes = new int [numberOfMaxElements];

        System.arraycopy(randomArray,randomArray.length - numberOfMaxElements,
                maxes, 0, numberOfMaxElements);

        System.out.println(Arrays.toString(maxes));

    }

    public static void inverse(final int[] randomArray) {
        for (int i = 0; i <= randomArray.length / 2 - 1 ; i++) {
            swap(randomArray, i, randomArray.length - 1 - i);
        }
    }

    public static void randomizeArray(int [] array, int low, int top){

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            int num;
            while (true) {
                num = random.nextInt(top - low + 1) + low;

                if (num / 10 != 0) {
                    break;
                }
            }

            array[i] = num;
        }
    }

    public static void randomizeArray(char [] array, char low, char top){
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            int num;
            num = random.nextInt(top - low + 1) + low;
            array[i] = (char) num;
        }
    }

    public static void swap(int [] array, int f, int s){
        if (f == s) return;

        if (!checkIndex(array, f)) return;
        if (!checkIndex(array, s)) return;

        int temp = array[f];
        array[f] = array[s];
        array[s] = temp;
    }

    private static boolean checkIndex(final int[] array, final int index) {
        if (index < 0 || index > array.length) {
            System.err.printf("Incorrect index: %d", index);
            return false;
        }
        return true;
    }

    public static void bubbleSort(int [] array){

        for (int i = 0; i < array.length; i++) {

            for (int j = 0; j < array.length - i - 1; j++) {

                if (array[j] > array[j + 1]) {
                    swap(array, j , j + 1);
                }

            }

        }

    }

    public static int search(int [] array, int number){
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number){
                return i;
            }
        }
        return -1;
    }

    public static int binarySearch(int [] array, int number){
        return binarySearch(array, 0, array.length - 1, number);
    }

    private static int binarySearch(int [] array, int left, int right, int number) {

        if (right == left){
            return array[right] == number ? right : -1;
        }

        int middle = (right + left) / 2;

        if (array [middle] == number) {
            return middle;
        }

        if (number > array[middle]){
            return binarySearch(array, middle + 1, right, number);
        } else {
            return binarySearch(array, left, middle - 1, number);
        }
    }
}
