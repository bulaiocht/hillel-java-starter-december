package arrays;

import java.util.Arrays;

public class SortingRunner {

    public static void main(String[] args) {

        int [] array = new int [2000000];

        HomeWorkRunner.randomizeArray(array, -3000000, 3000000);

//        System.out.println(Arrays.toString(array));

        int number = 42;

        int index = HomeWorkRunner.search(array, number);

//        if (index < 0) {
//            System.out.println("Number " + number + " not found");
//        } else {
//            System.out.println("Number " + number + " found on index " + index);
//        }

//        HomeWorkRunner.bubbleSort(array);

        Arrays.sort(array);

        int search = Arrays.binarySearch(array, number);

        if (search < 0) {
            System.out.println("Number " + number + " not found");
        } else {
            System.out.println("Number " + number + " found on index " + search);
        }


//        System.out.println(Arrays.toString(array));

    }

}
