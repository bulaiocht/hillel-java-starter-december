

package arrays;

import java.util.Arrays;
import java.util.Random;

public class BinarySearchRunner {

    public static void main(String[] args) {

        int[] array = new int[2000];

        randomize(array, -2000, 2000);

        int number = 42;

        Arrays.sort(array);

        int i = BinarySearcher.binarySearch(array, number);

        if (i == -1){
            System.out.printf("Number \"%d\" not found!\n", number);
        } else System.out.printf("Number \"%d\" found on index %d\n", number, i);


    }

    public static void randomize(int [] array, int min, int max){

        Random random = new Random();

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(max - min) + min;
        }
    }

}
