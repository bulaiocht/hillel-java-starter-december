

package arrays;

public class BinarySearcher {

    public static int binarySearch(int[] array, int number){

        return binarySearch(array, 0, array.length - 1, number);

    }

    private static int binarySearch(int [] array, int start, int end, int number){

        if (start == end){
            if (array[start] == number) {
                return start;
            } else return -1;
        }

        int mid = (start + end) / 2;

        if (array[mid] == number) return mid;

        if (number > array[mid]){
            return binarySearch(array, mid + 1, end, number);
        } else {
            return binarySearch(array, start, mid - 1, number);
        }

    }

}
