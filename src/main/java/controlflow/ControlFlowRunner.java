package controlflow;

import java.util.Arrays;

public class ControlFlowRunner {

    public static void main(String[] args) {

        System.out.println(Arrays.toString(args));

        if (args.length == 0 && args.length > 1 ){
            System.out.println("Wrong input. Good bye!");
            return;
        }

        String day = args[0];

        int num = Integer.parseInt(day);

//        evenIdentifier(num);
//        System.out.println("num: " + num);
//        num = increment(num);
//        System.out.println("num: " + num);

//        int [] array = new int[0];
//        int [] initialized = {2,4,6,8,10,12,14,16};
//
//        System.out.println(initialized[2]);
//        initialized[5] = 256;
//
//        System.out.println(initialized[5]);
//
//        System.out.println(initialized.length);

        switch (num){
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
            case 6:
                System.out.println("Saturday");
                break;
            case 7:
                System.out.println("Sunday");
                break;
            default:
                System.out.println("Incorrect day number");
                break;
        }

    }

    public static void evenIdentifier(int number) {
        System.out.println(number);
        if ( number % 2 != 0 ) {
            System.out.println("Number is not even");
        } else if (number == 0) {
            System.out.println("Number is ZERO");
        } else {
            System.out.println("Number is even");
        }
        System.out.println("end of method");
    }

    public static int increment(int num){
        return ++num;
    }
}
