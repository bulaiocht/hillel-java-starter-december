package io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public class TextFileReader {

    public static void main(String[] args) {

//        readTextFile();

//        bufferedReadFile();

//        bufferedWriteFile();

//        readerWriterFile();

        File root = new File("1");

        checkDir(root);

        CustomFileVisitor customFileVisitor = new CustomFileVisitor();

        try {

            Files.walkFileTree(root.toPath(), new CustomFileVisitor());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void checkDir(File file) {

        boolean directory = file.isDirectory();

        if (directory){

            File[] files = file.listFiles();

            if(files == null) return;

            for (File f : files) {

                if (f.isFile()){

                    String name = f.getName();

                    if (name.endsWith(".txt")){

                        readTextFile(f);

                    }

                } else {
                    checkDir(f);
                }

            }


        }
    }


    private static void readerWriterFile() {
        File existing = new File("simple-text.txt");

        File copy = new File("copy-" + existing.getName());

        BufferedReader reader = null;

        BufferedWriter writer = null;

        try {

            Files.deleteIfExists(copy.toPath());

            copy.createNewFile();

            reader = new BufferedReader(new FileReader(existing));

            writer = new BufferedWriter(new FileWriter(copy));

            String line = reader.readLine();

            while (line != null){

                writer.write(line);
                writer.newLine();
                writer.flush();
                line = reader.readLine();

            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void bufferedReadFile() {

        File file = new File("simple-text.txt");

        BufferedInputStream bis = null;

        byte [] buf = new byte[256];

        try {

            bis = new BufferedInputStream(new FileInputStream(file));
            int read = bis.read(buf);
            while (read != -1){
                System.out.print(new String(buf).trim());
                read = bis.read(buf);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeSilently(bis);
        }
    }

    private static void bufferedWriteFile(){

        File existing = new File("simple-text.txt");

        File copy = new File("copy-" + existing.getName());

        BufferedInputStream bis = null;

        BufferedOutputStream bos = null;

        byte buf [] = new byte [256];

        try {

            Files.deleteIfExists(copy.toPath());

            copy.createNewFile();

            bis = new BufferedInputStream(new FileInputStream(existing));

            bos = new BufferedOutputStream(new FileOutputStream(copy));

            int read = bis.read(buf);

            while (read != -1){

                bos.write(buf);
                bos.flush();

                read = bis.read(buf);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeSilently(bis);
            closeSilently(bos);
        }

    }

    private static void readTextFile() {

        File file = new File("simple-text.txt");

        System.out.println(file.exists());

        InputStream in = null;

        try {

            in = new FileInputStream(file);

            while (true){

                int b = in.read();

                if (b == -1) {
                    break;
                }

                System.out.print((char) b);

            }


        } catch (FileNotFoundException e) {

            System.err.println("There is no file found");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeSilently(in);
        }
    }

    public static void readTextFile(File file) {

        System.out.println(file.exists());

        InputStream in = null;

        try {

            in = new FileInputStream(file);

            while (true){

                int b = in.read();

                if (b == -1) {
                    break;
                }

                System.out.print((char) b);

            }


        } catch (FileNotFoundException e) {

            System.err.println("There is no file found");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeSilently(in);
        }
    }


    private static void closeSilently(Closeable closeable){
        try {
            if (closeable != null){
                closeable.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
