package inheritance;

public class Tiger extends Animal implements Jumpable, Swimmable {

    @Override
    public void sayPhrase() {
        System.out.println("Rawr!");
    }

    @Override
    public void jump() {
        System.out.println("I'm jumping on my prey!");
    }

    @Override
    public void swim() {
        System.out.println("Soo cool! Me like to swim!");
    }
}
