package inheritance;

public class Cat extends Animal implements Jumpable, Swimmable {

    @Override
    public void sayPhrase() {
        System.out.println("Miao!");
    }

    @Override
    public void jump() {
        System.out.println("I'm jumping on a sofa");
    }

    @Override
    public void swim() {
        System.out.println("Naah!");
    }
}
