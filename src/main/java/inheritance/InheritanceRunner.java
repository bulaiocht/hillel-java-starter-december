

package inheritance;

public class InheritanceRunner {

    public static void main(String[] args) {

        Animal animal1 = new Cat();

        Animal animal2 = new Tiger();

        animal1.sayPhrase();

        animal2.sayPhrase();

        animal1 = new Tiger();

        animal1.sayPhrase();

        Jumpable jumpable1 = new Cat();
        Jumpable jumpable2 = new Tiger();

        jumpable1.jump();
        jumpable2.jump();


        callSwimmables(new Cat());
        callSwimmables(new Tiger());

    }

    public static void callSwimmables(Swimmable swimmable){

        swimmable.swim();

    }

}
