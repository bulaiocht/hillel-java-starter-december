

package inheritance;

public class Animal {

    String phrase = "I'm an animal";

    public void sayPhrase(){

        System.out.println(this.phrase);

    }

}
