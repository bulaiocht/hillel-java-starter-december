package primitives;

public class PrimitiveRunner {

    public static void main(String[] args) {

//        byte byteNumber = 127;
//
//        short shortNumber = 1024;
//
//        int intNumber = 9_850_000;
//
//        long longNumber = 999999999999L;
//
//        char letterA = 'A';
//
//        char someSymbol = 1200;
//
//        System.out.println(someSymbol);
//
//        float floatNumber = 12.5f;
//
//        double doubleNum = 12.5;
//
//        System.out.println(doubleNum/13.756);
//
//        boolean boolNum = true;


//        byte b3 = (byte) 128;
//
//        System.out.println(b3);


        int a = 3;

        a++; //a = a + 1;

        ++a;

        System.out.println(a);

        a--;

        --a;

        System.out.println(a);


        System.out.println(3 * 4);

        System.out.println(4 / 3);

        System.out.println( 4 % 3 );

        int hex = 0x020B;

        int binary = 0b101;

        int octal = 07;

//        int shifted = binary << 1;
//
//        System.out.println(Integer.toBinaryString(shifted));
//
//        shifted = binary >> 1;
//
//        System.out.println(Integer.toBinaryString(shifted));
//
//        int minByte = -128;
//
//        System.out.println(Integer.toBinaryString(minByte));
//        System.out.println(minByte >>> 1);


//        System.out.println(2 > 3);
//
//        System.out.println( 2 < 3);
//
//        System.out.println(2 >= 3);

        boolean result1 = (17568 / 17) % 2 != 0;

        boolean result2 = (25*147) % 2 == 0;

        System.out.println(result1 || result2);

        System.out.println(result1 && result2);


        int r1 = (12 / 3) > 5 ? 2 : 1;

        System.out.println(r1);

        int c = 5;

        c *= 3; //c = c * 3; += -= /= %=

    }

}
