package intro;

/**
* This is coffee nachine model
*/
public class CoffeeMachine {

    String machineName;

    void grindBeans(){
        System.out.println("Grinding beans...");
    }

    void boilWater(){
        System.out.println("Pumping water from tank");
        System.out.println("Poring water to boiler");
        System.out.println("Boiling water...");
        System.out.println("Pumping hot water through coffee tray");
    }

    void brewCoffee(){
        System.out.println("Brewing coffee...");
    }

    void pourCoffee(){
        System.out.println("Pouring coffee...");
    }

    void takeMoney(){
        System.out.println("Taking your money...");
    }

    void giveChange(){
        System.out.println("Giving change...");
    }
}
