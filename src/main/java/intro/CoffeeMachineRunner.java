package intro;

public class CoffeeMachineRunner {

    public static void main(String[] args) {

        CoffeeMachine machine = new CoffeeMachine();

        machine.machineName = "Machine #1";

        CoffeeMachine machine1 = new CoffeeMachine();

        machine1.machineName = "Machine #2";

        machine = machine1;

        machine1.machineName = "Hello";

        System.out.println(machine.machineName);

        System.out.println(machine1.machineName);

//        System.out.println("Hello, let me brew you some coffee.");
//
//        machine.takeMoney();
//        machine.giveChange();
//        machine.boilWater();
//        machine.grindBeans();
//        machine.brewCoffee();
//        machine.pourCoffee();
//
//        System.out.println("Have a nice day!");
    }

}
