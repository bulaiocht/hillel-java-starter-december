package hw;

import arrays.HomeWorkRunner;

import java.util.Arrays;

public class HW3Runner {
    public static void main(String[] args) {

        taskOne();

        taskTwo();

    }

    private static void taskOne() {

        int [] ages = new int[1000];

        HomeWorkRunner.randomizeArray(ages, 12, 90);

        int adults = 0;
        int pensioners = 0;

        int adultAge = 16;
        int pensionAge = 60;

        adults = getQuantity(ages, adultAge);

        System.out.printf("There are %d adults in a list. \n", adults);

        pensioners = getQuantity(ages, pensionAge);

        System.out.printf("There are %d pensioners in a list. \n", pensioners);

        int teens = getQuantity(ages, 12, 16);

        double percentage = ((double) teens / pensioners) * 100;

        System.out.printf("The percentage of teens to pensioners is %.3f %% \n", percentage);
    }

    private static void taskTwo() {
        char [] letters = new char [26];

        HomeWorkRunner.randomizeArray(letters, 'a', 'z');

        System.out.println(Arrays.toString(letters));

        Arrays.sort(letters);

        System.out.println(Arrays.toString(letters));

        int uniques = countUniques(letters);

        System.out.printf("Number of unique letters: %d \n", uniques);

        char[] uniqueLetters = collectUniques(letters);

        System.out.println(Arrays.toString(uniqueLetters));

        char[] nameChars = "Evlampii".toLowerCase().toCharArray();

        char [] uniqueNameChars = collectUniques(nameChars);

        System.out.println(Arrays.toString(uniqueNameChars));

        for (int i = 0; i < uniqueNameChars.length; i++) {
            char current = uniqueNameChars[i];
            boolean notFound = true;
            for (int j = 0; j < uniqueLetters.length; j++) {
                if (current == uniqueLetters[j]){
                    notFound = false;
                }
            }
            if (notFound){
                System.out.println("You cannot write your name");
                break;
            }

        }
    }

    private static int countUniques(char [] array){

        int uniques = 1;

        char current = array[0];

        for (int i = 1; i < array.length; i++) {
            if (current != array[i]) {

                current = array[i];
                uniques++;

            }
        }

        return uniques;

    }

    private static char[] collectUniques(char [] array){

        Arrays.sort(array);

        int uniques = countUniques(array);

        char [] uniqueLetters = new char[uniques];

        int uniqueIndex = 0;

        uniqueLetters[uniqueIndex] = array[0];

        for (int i = 0; i < array.length; i++){
            if (uniqueLetters[uniqueIndex] != array[i]){
                uniqueIndex++;
                uniqueLetters[uniqueIndex] = array[i];
            }
        }

        return uniqueLetters;
    }

    private static int getQuantity(int[] array, int limit) {

        int quantity = 0;

        for (int i = 0; i < array.length; i++) {

            if (array[i] >= limit) {

                quantity++;

            }
        }

        return quantity;
    }

    private static int getQuantity(int[] array, int from, int to){

        int quantity = 0;

        for (int i = 0; i < array.length; i++) {

            if (array[i] >= from && array[i] <=to){

                quantity++;

            }

        }

        return quantity;
    }
}
