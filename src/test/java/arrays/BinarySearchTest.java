package arrays;

import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static org.assertj.core.api.Assertions.*;

public class BinarySearchTest {

    @Test
    public void binarySearchMethodTest() {

        int [] array = new int[100];

        HomeWorkRunner.randomizeArray(array, -99, 99);

        Arrays.sort(array);

        int index = new Random().nextInt(array.length);

        int number = array[index];

        int foundIndex = HomeWorkRunner.binarySearch(array, number);

        assertThat(foundIndex).isNotEqualTo(-1);

        assertThat(array[foundIndex]).isEqualTo(number);

    }
}
